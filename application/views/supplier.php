  <main role="main">
	  <div class="container-fluid">
		<div class="card">
		  <div class="card-body">
		  	<h4>Supplier Database</h4>
			<div class="float-left pb-3">
				<a class="btn btn-outline-info d-flex mt-2 px-4 mx-2" href="<?= base_url('APIsup')  ?>">Cek data API</a>
        <a href="<?= base_url('pakerest/Add_supp')?>"  class="btn btn-outline-success d-flex mt-2 px-4 mx-2">Tambah Supplier</a>
			</div>
			<div class="table-responsive">
			  <table class="table table-bordered">
				<thead>
				  <tr>
				    <td>No</td>
				    <td>Kode Supplier</td>
				    <td>Nama Supplier</td>
				    <td>Alamat Supplier</td>
				    <td>Telp Supplier</td>
            <td>Kota Supplier</td>
            <td>Setting</td>
				  </tr>
				</thead>
				<tbody>
          <?php 
          $no = 1;
          foreach ($datasupp as $supp){
          ?>
            <tr>
              <td><?= $no++ ?></td> 
              <td><?= $supp->kode_supplier ?></td> <!--Kode Supplier-->
              <td><?= $supp->nama_supplier ?></td> <!--Nama Supplier -->
              <td><?= $supp->alamat_supplier ?></td> <!--Lokasi Supplier -->
              <td><?= $supp->telp_supplier ?></td> <!-- Telp Supplier -->
              <td><?= $supp->kota_supplier ?></td> <!-- Kota Supplier -->
              <td>
                <div class="btn-group">
                  <a class="btn btn-outline-info" href="<?= base_url()?>pakerest/edit_supp/<?= $supp->kode_supplier ?>">Edit</a>
                  <a class="btn btn-outline-danger" href="<?= base_url()?>pakerest/delete_supp/<?= $supp->kode_supplier ?>" onclick="confirm('Apakah anda yakin menghapus data dengan nama <?= $supp->nama_supplier ?>?');">Hapus</a>
                </div>
              </td>
            </tr>
          <?php } ?>
				</tbody>
			  </table>
			</div>
		  </div>
		</div>
	  </div>
	</main>