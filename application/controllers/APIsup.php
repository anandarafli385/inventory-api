<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class APIsup extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    //Menampilkan data kontak
    function index_get() {
        $id = $this->get('kode_supplier');
        if ($id == '') {
            $supp = $this->db->get('supplier')->result();
        } else {
            $this->db->where('kode_supplier', $id);
            $supp = $this->db->get('supplier')->result();
        }
        $this->response($supp, 200);
    }


    //Mengirim atau menambah data kontak baru
    function index_post() {
        $data = array(
                    'kode_supplier'         => $this->post('kode_supplier'),
                    'nama_supplier'         => $this->post('nama_supplier'),
                    'alamat_supplier'       => $this->post('alamat_supplier'),
                    'telp_supplier'         => $this->post('telp_supplier'),
                    'kota_supplier'         => $this->post('kota_supplier'));
        $insert = $this->db->insert('supplier', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('api_status' => 'fail', 502));
        }
    }

     //Memperbarui data kontak yang telah ada
    function index_put() {
        $id = $this->put('kode_supplier');
        $data = array(
                    'kode_supplier'         => $this->put('kode_supplier'),
                    'nama_supplier'         => $this->put('nama_supplier'),
                    'alamat_supplier'       => $this->put('alamat_supplier'),
                    'telp_supplier'         => $this->put('telp_supplier'),
                    'kota_supplier'         => $this->put('kota_supplier'));
        $this->db->where('kode_supplier', $id);
        $update = $this->db->update('supplier', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('api_status' => 'fail', 502));
        }
    }

    //Menghapus salah satu data kontak
    function index_delete() {
        $id = $this->delete('kode_supplier');
        $this->db->where('kode_supplier', $id);
        $delete = $this->db->delete('supplier');
        if ($delete) {
            $this->response(array('api_status' => 'success'), 201);
        } else {
            $this->response(array('api_status' => 'fail', 502));
        }
    }
    
}
?>