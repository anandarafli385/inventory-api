<?php
Class Pakerest extends CI_Controller{
    
    var $API ="";
    
    function __construct() {
        parent::__construct();
        $this->API="http://localhost/inventory_api/APIsup";
        $this->load->library('curl');
        $this->load->model('Supplier_model');
    }
    
    // menampilkan data kontak
    function index(){
        $data['datasupp'] = json_decode($this->curl->simple_get($this->API.'/APIsup'));
        $this->load->view('includes/header');
        $this->load->view('supplier',$data);
        $this->load->view('includes/footer');
    }
    
    // insert data kontak
    function Add_Supp(){
        if(isset($_POST['submit'])){
            $data = array(
                    'kode_supplier'         => $this->post('kode_supplier'),
                    'nama_supplier'         => $this->post('nama_supplier'),
                    'alamat_supplier'       => $this->post('alamat_supplier'),
                    'telp_supplier'         => $this->post('telp_supplier'),
                    'kota_supplier'         => $this->post('kota_supplier'));
            $insert =  $this->curl->simple_post($this->API.'/APIsup', $data, array(CURLOPT_BUFFERSIZE => 10)); 
            if($insert)
            {
                $this->session->set_flashdata('hasil','Insert Data Berhasil');
            }else
            {
               $this->session->set_flashdata('hasil','Insert Data Gagal');
            }
            redirect('pakerest');
        }else{
            $this->load->view('includes/header');
            $this->load->view('action/tambahSupplier');
            $this->load->view('includes/footer');
        }
    }
    
    // edit data kontak
    function Edit_Supp($kode_supplier){
        if(isset($_POST['submit'])){
            $data = array(
                    'kode_supplier'         => $this->post('kode_supplier'),
                    'nama_supplier'         => $this->post('nama_supplier'),
                    'alamat_supplier'       => $this->post('alamat_supplier'),
                    'telp_supplier'         => $this->post('telp_supplier'),
                    'kota_supplier'         => $this->post('kota_supplier'));
            $update =  $this->curl->simple_put($this->API.'/APIsup', $data, array(CURLOPT_BUFFERSIZE => 10)); 
            if($update)
            {
                $this->session->set_flashdata('hasil','Update Data Berhasil');
            }else
            {
               $this->session->set_flashdata('hasil','Update Data Gagal');
            }
            redirect('pakerest');
        }else{
            $params = array('kode_supplier'=>  $this->uri->segment(4));
            $data['datasupp'] = json_decode($this->curl->simple_get($this->API.'/APIsup',$params));
            $data['supplier'] = $this->Supplier_model->getById($kode_supplier);
            $this->load->view('includes/header');
            $this->load->view('action/editSupplier',$data);
            $this->load->view('includes/footer');
        }
    }
    
    // delete data kontak
    function Delete_Supp($kode_supplier){
        if(empty($kode_supplier)){
            redirect('pakerest');
        }else{
            $delete =  $this->curl->simple_delete($this->API.'/APIsup', array('kode_supplier'=>$kode_supplier), array(CURLOPT_BUFFERSIZE => 10)); 
            if($delete)
            {
                $this->session->set_flashdata('hasil','Delete Data Berhasil');
            }else
            {
               $this->session->set_flashdata('hasil','Delete Data Gagal');
            }
            redirect('pakerest');
        }
    }

    function hapus_supplier($kode_supplier)
    {
        $this->Supplier_model->Delete($kode_supplier);
        redirect('pakerest');
    }

    function proses_tambah_supplier()
    {
        $this->Supplier_model->Add();
        redirect('pakerest');
    }

    function proses_edit_supplier()
    {
        $this->Supplier_model->Edit();
        redirect('pakerest');
    }

}